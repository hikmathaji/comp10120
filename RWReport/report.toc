\contentsline {section}{\numberline {1}Introduction to ethical frameworks}{3}{section.1}
\contentsline {section}{\numberline {2}ACM Code of Ethics and Professional Conduct}{3}{section.2}
\contentsline {section}{\numberline {3}Application of ACM Code of Ethics to ``Killer Robot'' Case}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Avoid harm to others}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Be honest and trustworthy}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Give proper credit for intellectual property}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Strive to achieve the highest quality}{4}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Accept and provide appropriate professional review}{5}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Manage personnel and resources to design and build information systems that enhance the quality of working life}{5}{subsection.3.6}
\contentsline {section}{\numberline {4}ACM Code of Ethics and other ethical frameworks}{5}{section.4}
