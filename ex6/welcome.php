<html>
<head>
<link rel = 'stylesheet' href = 'style.css' type = 'text/css'>
</head>
<body>
<div class='form-content'>
<?php

$name = isset($_POST['name'])?$_POST['name']:"";
$email = isset($_POST['email'])?$_POST['email']:"";
$dudes = array("Andrei", "Paddy", "Pranav", "Sebastian");

echo "<br><br>";
if(preg_match("/^[a-zA-Z ]*$/", $name)) 
{
	$found_a_dude = FALSE;
	foreach($dudes as $dude)
	{
		if(strcmp($dude, $name)==0)
		{
			echo "Hey, ".$name.", what's up!<br>";
			$found_a_dude = TRUE;
			break;
		}
	}
	if(!$found_a_dude)
		echo "Hello ".$name."!<br>";
}
else
{
	echo "Your name doesn't look like a name.<br>";
}
echo "<br><br>";
if(filter_var($email, FILTER_VALIDATE_EMAIL))
{
	if($found_a_dude)
		echo "What a terrific email you have, ".$name.".<br>";
	else
		echo "Your email looks fine.<br>";
} 
else
{
	echo "Your email is not actually an email.<br>";
}
?>
<p>Go <a href = 'form.html'>back</a></p>
</div>
</body>
</html>