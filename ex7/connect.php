<?php
 class DB {

    private static $db = NULL;
    public static $CONNECTION_STRING = "mysql:host=dbhost.cs.man.ac.uk;dbname=mbax2hh2;charset=utf8";
    public static $DB_USER = "mbax2hh2";
    public static $DB_PASS = "asdf1234";

    public static function getInstance() {
        if (is_null(self::$db)) {
            self::$db = new PDO(self::$CONNECTION_STRING, self::$DB_USER, self::$DB_PASS);
        }
        return self::$db;
    }
}
?>